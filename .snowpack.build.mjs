/** @type {import("snowpack").SnowpackUserConfig } */
export default {
  extends: "./.snowpack.config.mjs",
  mount: {
    src: "/",
  },
  optimize: {
    minify: true
  }
};
