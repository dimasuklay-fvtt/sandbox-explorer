import { moduleId } from "@util";
import { safeGame } from "@util/guards";
import {
  addExplorerButton,
  refreshExplorer,
  renderExplorer,
} from "./components/actions";
import { DataExplorer } from "./components/explorer";
import { initHandlebars } from "./components/handlebars";
import { initSettings } from "./components/settings";

const myapp: DataExplorer = new DataExplorer();

Hooks.once("init", async () => {
  initHandlebars();
  initSettings();
});

Hooks.once("ready", async () => {
  if (safeGame().settings.get(moduleId, "run-on-startup")) {
    renderExplorer.bind(myapp)();
  }
});

["renderActorDirectory", "renderItemDirectory"].forEach((hook) => {
  Hooks.on(hook, addExplorerButton.bind(myapp));
});

[
  "createActor",
  "updateActor",
  "deleteActor",
  "createItem",
  "updateItem",
  "deleteItem",
].forEach((hook) => {
  Hooks.on(hook, refreshExplorer.bind(myapp));
});

// https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/snowpack-env/index.d.ts
declare global {
  interface ImportMetaHot {
    accept(callback?: (update: { module: unknown }) => void): void;
    accept(
      dependencies: readonly string[],
      callback: (update: { module: unknown; deps: unknown[] }) => void
    ): void;
    dispose(callback: () => void): void;
    invalidate(): void;
    decline(): void;
    data: unknown;
  }
  interface ImportMeta {
    readonly hot: ImportMetaHot;
  }
}

if (import.meta.hot) {
  import.meta.hot.dispose(() => {
    console.log("import.meta.hot.dispose");
    console.log(import.meta.hot);
  });
  import.meta.hot.accept(({ module }) => {
    console.log("import.meta.hot.accept");
    console.log(import.meta.hot);
    console.log(module);
  });
}
