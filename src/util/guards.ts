import { _t } from "@util";

export function safeGame(): Game {
  if (game instanceof Game) {
    return game;
  } else {
    throw new Error(_t("error.uninitialized", { what: "game" }));
  }
}

export function safeUser(): User {
  let user = safeGame().user;

  if (user instanceof User) {
    return user;
  } else {
    throw new Error(_t("error.uninitialized", { what: "game.user" }));
  }
}

export function safeActors() {
  let actors = safeGame().actors;

  if (actors) {
    return actors;
  } else {
    throw new Error(_t("error.uninitialized", { what: "game.actors" }));
  }
}

export function safeItems() {
  let items = safeGame().items;

  if (items) {
    return items;
  } else {
    throw new Error(_t("error.uninitialized", { what: "game.items" }));
  }
}
