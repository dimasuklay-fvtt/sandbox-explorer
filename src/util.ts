import config from "./module.json";

import { safeGame } from "@util/guards";

const iconMap: Record<string, string> = {
  template: "fas fa-copy",
  sheettab: "fas fa-indent",
  multipanel: "fas fa-table",
  panel: "fas fa-columns",
  group: "fas fa-users",
  property: "fas fa-edit",
  citem: "fas fa-vector-square",
  arrow_left: "fas fa-angle-double-left",
  arrow_right: "fas fa-angle-double-right",
  character: "fas fa-copy",
  open: "fas fa-link",
  delete: "fas fa-trash",
  refresh: "fas fa-sync-alt",
  bug: "fas fa-bug",
  unknown: "fas fa-question",
};

export const moduleId = config.name;

export function _k(message: string) {
  return `${moduleId}.${message}`;
}

export function _t(
  message: string,
  options: Record<string, string | null> = {}
) {
  return safeGame().i18n.format(_k(message), options);
}

export function _i(icon: string) {
  return iconMap[icon?.toLowerCase()] || iconMap.unknown;
}
