import { _t, moduleId } from "@util";
import { safeGame } from "@util/guards";

export function initSettings() {
  safeGame().settings.register(moduleId, "run-on-startup", {
    name: _t("settings.run-on-startup"),
    hint: _t("settings.run-on-startup-hint"),
    scope: "world",
    config: true,
    type: Boolean,
    default: false,
  });

  safeGame().settings.register(moduleId, "show-summary", {
    name: _t("settings.show-summary"),
    hint: _t("settings.show-summary-hint"),
    scope: "world",
    config: false,
    type: Boolean,
    default: true,
  });
}
