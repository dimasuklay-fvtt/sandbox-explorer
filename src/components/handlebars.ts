import { _i, _t, moduleId } from "@util";

export const templates = {
  _explorer_data: `modules/${moduleId}/templates/partials/data.html`,
  _explorer_tab: `modules/${moduleId}/templates/partials/tab.html`,
  button: `modules/${moduleId}/templates/button.html`,
  explorer: `modules/${moduleId}/templates/explorer.html`,
};

export async function initHandlebars() {
  Handlebars.registerHelper(`${moduleId}-i18n`, _t);
  Handlebars.registerHelper(
    `${moduleId}-count`,
    (collection: Array<unknown> | Map<string, unknown>) => {
      return Array.isArray(collection) ? collection.length : collection?.size;
    }
  );
  Handlebars.registerHelper(`${moduleId}-icon`, (type: string) => {
    return `<i class="${_i(type)}"></i>`;
  });
  await loadTemplates(Object.values(templates));
}
