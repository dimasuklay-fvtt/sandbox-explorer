import { _i, _k, _t, moduleId } from "@util";
import { safeGame, safeActors, safeItems } from "@util/guards";
import {
  deleteComponent,
  showComponent,
  openComponentSheet,
  refreshTemplate,
} from "@components/actions";
import { templates } from "@components/handlebars";

export class DataExplorer extends Application {
  data?: Record<string, any>;

  static get defaultOptions() {
    const options: Partial<Application.Options> = {
      id: moduleId,
      template: templates.explorer,
      title: _k("app.title"),
      tabs: [
        {
          navSelector: ".tabs",
          contentSelector: ".content",
          initial: "character",
        },
      ],
      width: 720,
      height: 480,
      minimizable: true,
      resizable: true,
    };

    return mergeObject(super.defaultOptions, options) as Application.Options;
  }

  async _updateObject() {}

  _getHeaderButtons() {
    const buttons = super._getHeaderButtons(),
      bug_reporter = safeGame().modules.get("bug-reporter");

    if (bug_reporter?.active) {
      buttons.unshift({
        label: _t("buttons.bug"),
        class: `${moduleId}-bug`,
        icon: _i("bug"),
        onclick: () => {
          // @ts-ignore
          bug_reporter.api.bugWorkflow(moduleId);
        },
      });
    }

    return buttons;
  }

  activateListeners(html: JQuery<HTMLElement>) {
    html.find(".current.component").on("click", openComponentSheet);
    html.find(".linked.component").on("click", openComponentSheet);

    html.find(".nav.action").on("click", showComponent.bind(this));
    html.find(".delete.action").on("click", deleteComponent);
    html.find(".refresh.action").on("click", refreshTemplate);

    super.activateListeners(html);
  }

  getData() {
    this.data = this.refreshData();
    return this.data;
  }

  connectElements(
    elements: Record<string, any>,
    reference: Actor | Item,
    children: any[]
  ) {
    if (reference.id) {
      elements[reference.id] ||= {};
      elements[reference.id].node = reference;

      children.forEach((child) => {
        elements[child.id] ||= {};
        elements[child.id].references ||= [];
        elements[child.id].references.push(reference);
      });
    }
  }

  refreshData() {
    const element_types: string[] = [
        "template",
        "sheettab",
        "multipanel",
        "panel",
        "group",
        "property",
        "cItem",
      ],
      child_types: Record<string, string> = {
        template: "tabs",
        sheettab: "panels",
        multipanel: "panels",
        panel: "properties",
        group: "properties",
        cItem: "groups",
      },
      elements: Record<string, any> = {},
      categories: Record<string, any> = {};

    // process templates
    safeActors().contents.forEach((actor) => {
      const data = actor.data.data;

      if (data.istemplate) {
        categories.template ||= [];
        categories.template.push(actor.id);

        this.connectElements(elements, actor, data.tabs);
      }
    });

    // process other elements
    safeItems().contents.forEach((item) => {
      const type = item.data.type;

      if (element_types.includes(type)) {
        categories[type] ||= [];
        categories[type].push(item.id);

        const child_type = child_types[type],
          children = item.data.data[child_type] || [];

        this.connectElements(elements, item, children);
      }
    });

    return { categories: categories, elements: elements, templates: templates };
  }
}

type SandboxTemplate = Actor

type SandboxBaseItem = Item
type SandboxSheetTab = SandboxBaseItem
type SandboxMultipanel = SandboxBaseItem
type SandboxPanel = SandboxBaseItem
type SandboxGroup = SandboxBaseItem
type SandboxProperty = SandboxBaseItem
type SandboxCItem = SandboxBaseItem

type SandboxItem =
  | SandboxSheetTab
  | SandboxMultipanel
  | SandboxPanel
  | SandboxGroup
  | SandboxProperty
  | SandboxCItem;

interface SandboxTemplateDataProperties {
  type: "character";
  data: SandboxTemplateDataPropertiesData;
}

interface SandboxTemplateDataPropertiesData {
  istemplate: boolean;
  tabs: SandboxSheetTab[];
}

interface SandboxSheetTabDataProperties {
  type: "sheettab";
  data: SandboxSheetTabDataPropertiesData;
}

interface SandboxSheetTabDataPropertiesData {
  panels: SandboxPanel[];
}

interface SandboxMultipanelDataProperties {
  type: "multipanel";
  data: SandboxMultipanelDataPropertiesData;
}

interface SandboxMultipanelDataPropertiesData {
  panels: SandboxPanel[];
}

interface SandboxPanelDataProperties {
  type: "panel";
  data: SandboxPanelDataPropertiesData;
}

interface SandboxPanelDataPropertiesData {
  properties: SandboxProperty[];
}

interface SandboxGroupDataProperties {
  type: "group";
  data: SandboxGroupDataPropertiesData;
}

interface SandboxGroupDataPropertiesData {
  properties: SandboxProperty[];
}

interface SandboxPropertyDataProperties {
  type: "property";
  data: SandboxPropertyDataPropertiesData;
}

interface SandboxPropertyDataPropertiesData {}

interface SandboxCItemDataProperties {
  type: "cItem";
  data: SandboxCItemDataPropertiesData;
}

interface SandboxCItemDataPropertiesData {
  groups: SandboxGroup[];
}

type SandboxActorDataProperties = SandboxTemplateDataProperties;
type SandboxItemDataProperties =
  | SandboxSheetTabDataProperties
  | SandboxMultipanelDataProperties
  | SandboxPanelDataProperties
  | SandboxGroupDataProperties
  | SandboxPropertyDataProperties
  | SandboxCItemDataProperties;

declare global {
  interface DocumentClassConfig {
    Actor: SandboxTemplate;
    Item: SandboxItem;
  }
  interface DataConfig {
    Actor: SandboxActorDataProperties;
    Items: SandboxItemDataProperties;
  }
}
