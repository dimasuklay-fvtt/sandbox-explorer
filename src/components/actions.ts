import { _t, moduleId } from "@util";
import { safeGame, safeUser, safeActors } from "@util/guards";
import { templates } from "./handlebars";
import { DataExplorer } from "./explorer";

export async function addExplorerButton(
  this: DataExplorer,
  app: Application,
  html: JQuery<HTMLElement>
) {
  if (safeUser().isGM) {
    const explorer = html
      .constructor(await renderTemplate(templates.button, { m: moduleId }))
      .click(renderExplorer.bind(this));
    if (html.find(`div.header-actions.${moduleId}`).length === 0) {
      html.find("div.header-actions").before(explorer);
    }
  }
}

export function deleteComponent(e: JQuery.TriggeredEvent) {
  const clicked = $(e.currentTarget),
    component_id = clicked.data("id"),
    component_type =
      clicked.data("type") === "character" ? "template" : clicked.data("type"),
    collection =
      component_type === "template" ? safeGame().actors : safeGame().items,
    component = collection?.get(component_id);

  if (component) {
    Dialog.confirm({
      title: _t("dialogs.delete.title"),
      content: _t("dialogs.delete.prompt", {
        name: component.name,
        type: _t(`components.${component_type}`),
      }),
      yes: async () => {
        component?.delete();
      },
    });
  }
}

export function showComponent(this: DataExplorer, e: JQuery.TriggeredEvent) {
  const clicked = $(e.currentTarget),
    id = clicked.data("id"),
    tab = clicked.data("type"),
    html = this.element,
    target = html.find(".current-item").filter(`[data-id=${id}]`)[0],
    row = $(target).closest("tr");

  this._tabs[0].activate(tab);
  target.scrollIntoView({
    behavior: "smooth",
    block: "end",
  });
  row.animate({ opacity: 0 }, 100).animate({ opacity: 1 }, 200);
}

export function openComponentSheet(e: JQuery.TriggeredEvent) {
  const clicked = $(e.currentTarget),
    collection =
      clicked.data("type") === "character"
        ? safeGame().actors
        : safeGame().items,
    id: string = clicked.data("id"),
    sheet = collection?.get(id)?.sheet;

  sheet?.render(true);
  sheet?.bringToTop();
}

export function refreshExplorer(this: DataExplorer) {
  if (this.rendered) {
    this.data = this.refreshData();
    this.render();
  }
}

export function refreshTemplate(e: JQuery.TriggeredEvent) {
  const clicked = $(e.currentTarget),
    component = safeActors().get(clicked.data("id"));

  component.sheet.buildSheet();
}

export async function renderExplorer(
  this: DataExplorer,
  render = true
) {
  if (safeUser().isGM) {
    this.render(render);
  }
}
