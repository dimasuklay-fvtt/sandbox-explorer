# Sandbox Explorer

Explore the configuration of a world created with the Sandbox System Builder.

## Features

- View all Sandbox components and how they link together
- Identify broken references
- Identify empty or unlinked components
- Refresh templates from the explorer
- Open or delete any component from the explorer

## Installation

Manifest URL: https://gitlab.com/api/v4/projects/27683785/packages/generic/sandbox-explorer/latest/module.json

## Usage

NOTE: The explorer is only visible to GMs.

Once you activate the module, a "Sandbox Explorer" button will appear at the top of the Actor and Item Directories.

![The Sandbox Explorer button at the top of the Actors Directory](docs/screenshot-explorer-button.jpg)

You can also toggle a setting to open the explorer automatically on startup.

Inside the explorer, click on any component to open its configuration sheet, or click on the arrows to go to its explorer entry.

## Change Log

See [CHANGELOG.md](CHANGELOG.md).
