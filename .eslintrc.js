/**
 * Loads https://github.com/typhonjs-fvtt/eslint-config-foundry.js/blob/main/0.8.0.js
 * NPM: https://www.npmjs.com/package/@typhonjs-fvtt/eslint-config-foundry.js
 *
 * Note: specific versions are located in /<VERSION>
 */
module.exports = {
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint"],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "@typhonjs-fvtt/eslint-config-foundry.js",
  ],
  rules: {
    "@typescript-eslint/no-shadow": [
      "error",
      { builtinGlobals: true, hoist: "all", allow: ["event"] },
    ],
  },
};
