// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

import proxy from "http2-proxy"

import { config as loadEnv } from "dotenv-safe"
import { createRequire } from "module"

loadEnv({ example: ".env.dev.example" })

const require = createRequire(import.meta.url)
const pkg = require("./package.json");

const mypath = `/${process.env.PACKAGE_TYPE}s/${pkg.name}/`

/** @type {import("snowpack").SnowpackUserConfig } */
export default {
  extends: "./.snowpack.config.mjs",
  mount: {
    src: mypath,
  },
  routes: [
    {
      src: '^/socket.io/.*',
      upgrade: (req, socket, head) => {
        return proxy.ws(req, socket, head, {
          hostname: process.env.FOUNDRY_HOSTNAME,
          port: process.env.FOUNDRY_PORT,
          protocol: process.env.FOUNDRY_PROTOCOL
        })
      }
    },
    {
      /** Proxy everything except this package to the Foundry server */
      src: `^(?!${mypath}).*`,
      dest: (req, res) => {
        return proxy.web(req, res, {
          hostname: process.env.FOUNDRY_HOSTNAME,
          port: process.env.FOUNDRY_PORT,
          protocol: process.env.FOUNDRY_PROTOCOL
        })
      }
    }
  ],
  devOptions: {
    open: "none",
  },
};
