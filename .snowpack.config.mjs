// Snowpack Configuration File
// See all supported options: https://www.snowpack.dev/reference/configuration

import { config as loadEnv } from "dotenv-safe"
import { createRequire } from "module"

const require = createRequire(import.meta.url)
const pkg = require("./package.json");

loadEnv()

const plugins = [
  [
    "./plugins/snowpack-replace.js",
    {
      extensions: [ ".js", ".json", ".css", ".html" ],
      transforms: {
        "__PKG_NAME__": pkg.name,
        "__PKG_DESC__": pkg.description,
        "__PKG_VERSION__": pkg.version,
        "__PKG_AUTHOR__": pkg.author,
        "__PKG_HOMEPAGE__": pkg.homepage,
      }
    }
  ],
  "@snowpack/plugin-dotenv",
  "@snowpack/plugin-typescript",
  "snowpack-plugin-hmr-inject",
]

if (process.env.CI) {
  plugins.push([
    "snowpack-plugin-zip",
    { outputPath: `./${pkg.name}.zip` }
  ])
}

/** @type {import("snowpack").SnowpackUserConfig } */
export default {
  alias: {
    "@util": "./src/util",
    "@components": "./src/components"
  },
  plugins,
  packageOptions: {
    types: true,
    polyfillNode: true,
  },
  devOptions: {
    open: "none",
  },
  buildOptions: {
    clean: true,
    htmlFragments: true,
    out: process.env.DEPLOY_PATH,
  },
  optimize: {
    entrypoints: ["main.js"],
    minify: false,
    target: "es2020"
  },
};
