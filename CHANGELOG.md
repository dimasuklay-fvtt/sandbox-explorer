# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.4.4](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.4.3...v1.4.4) (2021-07-18)

### Styles

- add hover style to action elements ([18d9023](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/18d9023610e247c14ecfbf442fd9e50be840e220))

### [1.4.3](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.4.2...v1.4.3) (2021-07-17)

### Bug Fixes

- check whether bug-reporter is active, not just installed ([f43b84c](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/f43b84c3937131399277c2ca365ebd5deffde287))

### [1.4.2](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.4.1...v1.4.2) (2021-07-13)

### Bug Fixes

- replace redundant tools button with a bug report button ([ac2a309](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/ac2a309ab22890837a1924f81f8978b685ef58b5))

### [1.4.1](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.4.0...v1.4.1) (2021-07-09)

### Bug Fixes

- restore optional chaining to guard against null types ([d57313a](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/d57313ae4ae459e16dd8bfb0b12dcc2712987e17))

## [1.4.0](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.3.1...v1.4.0) (2021-07-08)

### Features

- delete a component from the explorer UI ([3d69f7f](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/3d69f7fc24995353f97f7ba082ea7569db7d024f))
- trigger template refresh from the explorer UI ([b4c435a](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/b4c435a1365d200064d8985c393d60e78dc3f9a4))

### Bug Fixes

- **ui:** align explorer button with other directory action buttons ([f6ee9fa](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/f6ee9faf5cb0daa6e143a7ba251f10e6add3b380))
- **ui:** ensure that explorer buttons are created only once ([79b1b28](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/79b1b28c6f7d66e19edf418b1653cd01a04468d0))
- **ui:** suppress the child column if not supported by the component type ([886d30c](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/886d30c76ca35c03676efb716723139b9c8fbc87))

### [1.3.1](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.3.0...v1.3.1) (2021-07-06)

### Bug Fixes

- **ui:** don't scroll the entire canvas when following links ([7a46b0d](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/7a46b0d786d9b079658ea73f60b403ec2c8f7c35))

## [1.3.0](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.2.0...v1.3.0) (2021-07-05)

### Features

- refresh explorer data when actors or items are updated ([0aa1ee4](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/0aa1ee4c86ad2032138d2eedca7c69517d237eac))

## [1.2.0](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.1.2...v1.2.0) (2021-07-05)

### Features

- namespace handlebars helpers ([7230ae2](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/7230ae2b8f9cb2433499255c2adce3fd399dffc9))
- **ui:** support opening the item sheet or navigating to the explorer entry of a component reference ([fb2a7f8](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/fb2a7f828f68ecf99421f396627c2552089f1618))

### [1.1.2](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.1.1...v1.1.2) (2021-07-05)

### Bug Fixes

- match module name in localization strings ([45b2fa7](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/45b2fa7626e1895987c3c7a8517df875f63cc6c1))

### [1.1.1](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.1.0...v1.1.1) (2021-07-05)

### Bug Fixes

- **ui:** fix column alignment for components without references ([7d767a1](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/7d767a137c91111ac795ac5f9ab4e3db50c34e42))

## [1.1.0](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.0.1...v1.1.0) (2021-07-04)

### Features

- add clickable elements ([66e2585](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/66e2585a8bac8c12639adc4092d0503e4ad69aa7))

### [1.0.1](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/compare/v1.0.0...v1.0.1) (2021-07-04)

### Bug Fixes

- set the correct manifest and download URLs ([c04f9f5](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/c04f9f541dc5c8867f759c6f507712c153eaf423))

## 1.0.0 (2021-07-04)

### Features

- display # of cItems in groups tab instead of the complete list ([9a1e5e2](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/9a1e5e230785888e254f23e1074277cedb53acff))
- initial release ([bc16166](https://gitlab.com/dimasuklay-fvtt/sandbox-explorer/commit/bc161661e76f66963cc0cb1d0e3e1fa1a6efd866))
