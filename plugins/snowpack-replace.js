module.exports = function (config, options) {
  return {
    name: "snowpack-replace",
    transform({ contents, fileExt }) {
      let excluded = !options.extensions?.includes(fileExt);

      return excluded
        ? contents
        : Object.entries(options.transforms).reduce(
            (str, [key, value]) => str.replace(new RegExp(key, "g"), value),
            contents
          );
    },
  };
};
